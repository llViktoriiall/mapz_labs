﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Collections;
using Newtonsoft.Json;



namespace Game_0._1
{
    public partial class Form3 : Form
    {
        int Und = 16;
        int Gold = 0;
        int Iron = 0;
        int Coal = 0;
        public Form3()
        {
            InitializeComponent();
            pictureBox1.Image = Properties.Resources.map;
            pictureBox2.Image = Properties.Resources.rec_map;
            pictureBox3.Image = Properties.Resources.rec_map;
            pictureBox4.Image = Properties.Resources.rec_map;
            pictureBox5.Image = Properties.Resources.rec_map;
            pictureBox6.Image = Properties.Resources.rec_map;
            pictureBox7.Image = Properties.Resources.rec_map;
            pictureBox8.Image = Properties.Resources.rec_map;
            pictureBox9.Image = Properties.Resources.rec_map;
            pictureBox10.Image = Properties.Resources.rec_map;
            pictureBox11.Image = Properties.Resources.rec_map;
            pictureBox12.Image = Properties.Resources.rec_map;
            pictureBox13.Image = Properties.Resources.rec_map;
            pictureBox14.Image = Properties.Resources.rec_map;
            pictureBox15.Image = Properties.Resources.rec_map;
            pictureBox16.Image = Properties.Resources.rec_map;
            pictureBox17.Image = Properties.Resources.rec_map;
        }

        FlyWeightFactory fw = new FlyWeightFactory();
        Creator client = new Creator();
        private void button1_Click(object sender, EventArgs e)
        {
            string info = "";
            Thread process1 = new Thread(() =>
            {
                Singleton.GetInstance(6, 4, "Ivan2008");
                info = "Hello, " + Singleton.name + ". You are at level " + Singleton.level + " and you already have played " + Singleton.played + " games.\n";
            });
            Thread process2 = new Thread(() =>
            {
                Singleton.GetInstance(7, 7, "SuperPuperOliia");
                info = "Hello, " + Singleton.name + ". You are at level " + Singleton.level + " and you already have played " + Singleton.played + " games.\n";
            });
            richTextBox1.Text = "Thread-safe singleton:\n";
            process1.Start();
            process2.Start();
            process1.Join();
            richTextBox1.Text = richTextBox1.Text + "The singleton after first thread: \n";
            richTextBox1.Text = richTextBox1.Text + info;
            process2.Join();
            richTextBox1.Text = richTextBox1.Text + "The singleton after second thread: \n";
            richTextBox1.Text = richTextBox1.Text + info;




            richTextBox1.Text = richTextBox1.Text + "\nLazy singleton: \n";
            LazySingleton s1 = LazySingleton.Instance();
            LazySingleton s2 = LazySingleton.Instance();
            s1.set_t(30);
            int time = s2.get_t();
            richTextBox1.Text = richTextBox1.Text + "You have " + time + " minutes to complete the round\n";




            richTextBox1.Text = richTextBox1.Text + "\nAbstract fabric: \n";
            FactoryClient s = new FactoryClient();
            s.M_f();
            string t = s.info;
            richTextBox1.Text = richTextBox1.Text + "You have " + s.count2 + " " + t + "\n";
            s.S_m();
            t = s.info;
            richTextBox1.Text = richTextBox1.Text + "You have " + s.count1 + " " + t + "\n";
            s.S_f();
            s.S_f();
            t = s.info;
            richTextBox1.Text = richTextBox1.Text + "You have " + s.count3 + " " + t + "\n";




            richTextBox1.Text = richTextBox1.Text + "\nPrototype: \nOn this level you have next recources: \n";
            Recource r1 = new Recource();
            r1.name = "Oil";
            r1.amount = 0.55;
            r1.price = 6.2;
            string inf = r1.name + " in amount of " + r1.amount + " with price: " + r1.price + "\n";
            richTextBox1.Text = richTextBox1.Text + inf;
            Recource r2 = r1.Copy();
            Recource r3 = r1.Copy();
            r2.name = "Gold";
            inf = r2.name + " in amount of " + r2.amount + " with price: " + r2.price + "\n";
            richTextBox1.Text = richTextBox1.Text + inf;
            r3.price = 0.77;
            inf = r3.name + " in amount of " + r3.amount + " with price: " + r3.price + "\n";
            richTextBox1.Text = richTextBox1.Text + inf;




            richTextBox1.Text = richTextBox1.Text + "\nDecorator: \n";
            client.createAlloy("Alloy1", "Iron");
            client.addAdditives("Gold");
            client.addAdditives("Zink");
            string alloy1 = client.info;
            richTextBox1.Text = richTextBox1.Text + alloy1 + " and its price is " + client.price + "\n";
            client.createAlloy("Alloy2", "Tin");
            client.addAdditives("Nikel");
            string alloy2 = client.info;
            richTextBox1.Text = richTextBox1.Text + alloy2 + " and its price is " + client.price + "\n";



            Disasters disasters = new Disasters();
            Money money = new Money();
            Achievments achievements = new Achievments();
            List<ICommand> commands = new List<ICommand>
            {
                new DisasterCommand(disasters, "F"),
                new MoneyCommand(money, "M"),
                new MoneyCommand(money, "M"),
                new MoneyCommand(money, "M"),
                new AchieveCommand(achievements,"EXP"),
                new AchieveCommand(achievements,"GD")
            };
            Manager manager = new Manager();
            manager.SetCommand(new MacroCommand(commands));
            manager.DoProject();
            richTextBox1.Text = richTextBox1.Text + manager.info;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
           //richTextBox1.Text = richTextBox1.Text + "OK";
        }

        info current = new info();
        private void pictureBox4_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = richTextBox1.Text + "\nYou have found:\n";
            pictureBox4.Image = Properties.Resources.gold;
            FlyWeight gold = fw.GetCharacter("Gold");
            gold.setVolume(20);
            double temp;
            temp = gold.getVolume();
            string info = gold.getInfo();
            richTextBox1.Text = richTextBox1.Text + info + "\nVolume:" + temp;
            Und--;
            Gold++;
            current.unDiscovered = Und;
            current.gold = Gold;
            richTextBox1.Text = richTextBox1.Text + "\nYou have found gold!\n";
            string saved;
            saved = "\nCurrent state:\n";
            saved += "Undiscovered parts of the map - " + Und + "\n";
            saved += "Gold - " + Gold + "\n";
            saved += "Iron - " + Iron + "\n";
            saved += "Coal - " + Coal + "\n";
            richTextBox1.Text = richTextBox1.Text + saved;
        }

        private void pictureBox14_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = richTextBox1.Text + "\nYou have found:\n";
            pictureBox14.Image = Properties.Resources.coal;
            FlyWeight coal = fw.GetCharacter("Coal");
            coal.setVolume(10);
            double temp;
            temp = coal.getVolume();
            string info = coal.getInfo();
            richTextBox1.Text = richTextBox1.Text + info + "\nVolume:" + temp;
            Und--;
            Coal++;
            current.unDiscovered = Und;
            current.coal = Coal;
            richTextBox1.Text = richTextBox1.Text + "\nYou have found coal!\n";
            string saved;
            saved = "\nCurrent state:\n";
            saved += "Undiscovered parts of the map - " + Und + "\n";
            saved += "Gold - " + Gold + "\n";
            saved += "Iron - " + Iron + "\n";
            saved += "Coal - " + Coal + "\n";
            richTextBox1.Text = richTextBox1.Text + saved;
        }

        private void pictureBox16_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = richTextBox1.Text + "\nYou have found:\n";
            pictureBox16.Image = Properties.Resources.iron;
            FlyWeight iron = fw.GetCharacter("Iron");
            iron.setVolume(15);
            double temp;
            temp = iron.getVolume();
            string info = iron.getInfo();
            richTextBox1.Text = richTextBox1.Text + info + "\nVolume:" + temp;
            Und--;
            Iron++;
            current.unDiscovered = Und;
            current.iron = Iron;
            richTextBox1.Text = richTextBox1.Text + "\nYou have found iron!\n";
            string saved;
            saved = "\nCurrent state:\n";
            saved += "Undiscovered parts of the map - " + Und + "\n";
            saved += "Gold - " + Gold + "\n";
            saved += "Iron - " + Iron + "\n";
            saved += "Coal - " + Coal + "\n";
            richTextBox1.Text = richTextBox1.Text + saved;
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = richTextBox1.Text + "\nFlyWeight: \n";
            pictureBox5.Image = Properties.Resources.gold;
            FlyWeight gold = fw.GetCharacter("Gold");
            gold.setVolume(50);
            double temp;
            temp = gold.getVolume();
            string info = gold.getInfo();
            richTextBox1.Text = richTextBox1.Text + info + "\nVolume:" + temp;
            Und--;
            Gold++;
            current.unDiscovered = Und;
            current.gold = Gold;
            richTextBox1.Text = richTextBox1.Text + "\nYou have found gold!\n";
            string saved;
            saved = "\nCurrent state:\n";
            saved += "Undiscovered parts of the map - " + Und + "\n";
            saved += "Gold - " + Gold + "\n";
            saved += "Iron - " + Iron + "\n";
            saved += "Coal - " + Coal + "\n";
            richTextBox1.Text = richTextBox1.Text + saved;
        }   

        private void button2_Click(object sender, EventArgs e)
        {
            Facade facade = new Facade(fw, client);
            string check = facade.Operations();
            richTextBox1.Text = richTextBox1.Text + check;
        }

        Originator o = new Originator();
        private void button3_Click(object sender, EventArgs e)
        {
            o.CreateMemento(current);
            string temp = o.getSaved();
            richTextBox1.Text = richTextBox1.Text + temp;                    
        }

        private void button4_Click(object sender, EventArgs e)
        {
            current = JsonConvert.DeserializeObject<info>(o.getSaved());
            richTextBox1.Text = richTextBox1.Text + "Everything is back!";
            Und = current.unDiscovered;
            Gold = current.gold;
            Iron = current.iron;
            Coal = current.coal;
            richTextBox1.Text = richTextBox1.Text + "\nUndiscovered" + current.unDiscovered + "\nGold" + current.gold + "\nIron" + current.iron + "\nCoal" + current.coal;
        }
        Context c = new Context(new ConcreteStateA());
        ConcreteStateA st1 = new ConcreteStateA();
        ConcreteStateB st2 = new ConcreteStateB();
        ConcreteStateC st3 = new ConcreteStateC();
        int counter = 0;
        private void pictureBox12_Click(object sender, EventArgs e)
        {
            string info;
            counter++;
            switch (counter)
            {
                case 1:
                    c.TransitionTo(st1);
                    pictureBox12.Image = Properties.Resources.b1;
                    info = c.Request1();
                    richTextBox1.Text = richTextBox1.Text + info;

                    break;
                case 2:
                    c.TransitionTo(st2);
                    pictureBox12.Image = Properties.Resources.b2;
                    info = c.Request2();
                    richTextBox1.Text = richTextBox1.Text + info;
                    break;
                case 3:
                    c.TransitionTo(st3);
                    pictureBox12.Image = Properties.Resources.b3;
                    info = c.Request3();
                    richTextBox1.Text = richTextBox1.Text + info;
                    break;
            }


        }

        private void button5_Click(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }
        Subject sub1 = new Subject();
        Observer obs1;
        private void button6_Click(object sender, EventArgs e)
        {
            obs1 = new Observer("TradeMarket", sub1);
            if (checkBox3.Checked)
            {
                sub1.increaseCoeff(0.1);
                richTextBox1.Text = richTextBox1.Text + "\nYour trade coeff is: " + sub1.getCoeff();
                obs1.SaveSubject(sub1);
                richTextBox1.Text = richTextBox1.Text + "\nChanges applied a\n";
                richTextBox1.Text = richTextBox1.Text + "\nSo you can sell with price: " + obs1.Sell(sub1.getCoeff());
            }
            if(checkBox2.Checked)
            {
                sub1.decreaseCoeff(0.1);
                richTextBox1.Text = richTextBox1.Text + "\nYour trade coeff is: " + sub1.getCoeff();
                obs1.SaveSubject(sub1);
                richTextBox1.Text = richTextBox1.Text + "\nChanges applied at\n";
                richTextBox1.Text = richTextBox1.Text + "\nSo you can sell with price: " + obs1.Sell(sub1.getCoeff());
            }
        }
    }








    //поведінкові шаблони2





    //Або шось аля консолькі з чітами, або шось інше придумати


    interface ICommand
    {
        string Do();//робить
        string Undo();//вертає
    }
    class MacroCommand : ICommand
    {
        string Info = "";
        List<ICommand> commands;
        public MacroCommand(List<ICommand> coms)
        {
            commands = coms;//список
        }
        public string  Do()
        {
            foreach (ICommand c in commands)
                Info+=c.Do();
            return Info;
        }

        public string Undo()
        {
            foreach (ICommand c in commands)
                Info+=c.Undo();
            return Info;
        }
    }

    class Achievments
    {
        public string GoldDigger(bool _do)
        {
            if(_do)
            {
                return "\nYou have found a lot of gold? so you are a Gold Digger now!\n";
            }
            else
            {
                return "\nThe achievement was removed\n";
            }
        }
        public string Explorer(bool _do)
        {
            if (_do)
            {
                return "\nYou have explored a lot of maps, we give you achevement Explorer!\n";
            }
            else
            {
                return "\nThe achievement was removed\n";
            }
        }
    }

    class Disasters
    {
        public string EarthShake(bool _do)
        {
            if (_do)
            {
                return "\nErthshake has changed recources location\n";
            }
            else
            {
                return "\nGod came to us and repaired everything\n";
            }
        }
        public string Fire(bool _do)
        {
            if (_do)
            {
                return "\nOne of your buildings is on fire now!\n";
            }
            else
            {
                return "\nLuckily, fire didn't destrouy you building\n";
            }
        }
    }

    class Money
    {
        public string More(bool _do)
        {
            if (_do)
            {
                return "\n+500 coins\n";
            }
            else
            {
                return "\n-500 coins\n";
            }
        }
        public string Less(bool _do)
        {
            if (_do)
            {
                return "\n-500 coins\n";
            }
            else
            {
                return "\n+500 coins\n";
            }
        }
    }

    class AchieveCommand : ICommand
    {
        Achievments achievment;
        string check;
        string info="";
        public AchieveCommand(Achievments a,string temp)
        {
            achievment = a;
            check = temp;
        }
        public string Do()
        {
            if(check =="GD")
            {
                info += achievment.GoldDigger(true);
            }
            if(check == "EXP")
            {
                info += achievment.Explorer(true);
            }
            return info;
        }
        public string Undo()
        {
            if (check == "GD")
            {
                info += achievment.GoldDigger(false);
            }
            if (check == "EXP")
            {
                info += achievment.Explorer(false);
            }
            return info;
        }
    }

    class DisasterCommand : ICommand
    {
        Disasters disaster;
        string check;
        string info;

        public DisasterCommand(Disasters d, string temp)
        {
            disaster = d;
            check = temp;
        }
        public string Do()
        {
            if (check == "ESH")
            {
                info += disaster.EarthShake(true);
            }
            if (check == "F")
            {
                info += disaster.Fire(true);
            }
            return info;
        }
        public string Undo()
        {
            if (check == "ESH")
            {
                info += disaster.EarthShake(false);
            }
            if (check == "F")
            {
                info += disaster.Fire(false);
            }
            return info;
        }
    }

    class MoneyCommand : ICommand
    {
        Money money;
        string check;
        string info;
        public MoneyCommand(Money m, string temp)
        {
            money = m;
            check = temp;
        }
        public string Do()
        {
            if (check == "M")
            {
                info += money.More(true);
            }
            if (check == "L")
            {
                info += money.Less(true);
            }
            return info;
        }
        public string Undo()
        {
            if (check == "M")
            {
                info += money.More(false);
            }
            if (check == "L")
            {
                info += money.Less(false);
            }
            return info;
        }
    }

    class Manager
    {
        public string info = "";
        ICommand command;
        public void SetCommand(ICommand com)
        {
            command = com;
        }
        public void DoProject()
        {
            if (command != null)
                info += command.Do();
        }
        public void UndoProject()
        {

            if (command != null)
                info +=command.Undo();
        }
    }










    //апдейт клоли змінюється коефіцієнт торгівлі


    class Observer
    {
        Subject sState;
        double sell_price = 5.78;
        public string Title { get; set; }
        public Observer(string title, Subject subject)
        {
            if (string.IsNullOrEmpty(title))
            {
                throw new ArgumentNullException(nameof(title));
            }
            if (subject == null)
            {
                throw new ArgumentNullException(nameof(subject));
            }
            Title = title;
            sState = subject;
        }
        public bool SaveSubject(Subject subject)
        {
            if(sState != subject)
            {
                sState = subject;
                return true;
            }
            else
            {
                return false;
            }
        }
        public double Sell(double coeff)
        {
            double price = sell_price * coeff;
            return price;
        }
    }

    class Subject
    {
        private double sell_coeff = 0.75;
        public void setCoeff(double temp)
        {
            sell_coeff = temp;
        }

        public void increaseCoeff(double temp)
        {
            sell_coeff += temp;
        }

        public void decreaseCoeff(double temp)
        {
            sell_coeff -= temp;
        }

        public double getCoeff()
        {
            return sell_coeff;
        }
    }

          




    //поведінкові шаблони1



    //будівля з трьома станами: будується, збудована, модернізована

    class Context
    {
        private State _state = null;

        public Context(State state)
        {
            this.TransitionTo(state);
        }
        public void TransitionTo(State state)
        {
            this._state = state;
            this._state.SetContext(this);
        }        
        public string Request1()
        {
            this._state.ShowState();

            return "\nYour factory is in building process.\nIt is producing with speed "+_state.time;
        }

        public string Request2()
        {
            this._state.ShowState();
            return "\nYour factory has been builded.\nIt is producing with speed " + _state.time;
        }

        public string Request3()
        {
            this._state.ShowState();
            return "\nYour factory is updated now.\nIt is producing with speed " + _state.time;
        }

    }
    
    abstract class State
    {
        protected Context _context;
        public int time;

        public void SetContext(Context context)
        {
            this._context = context;
        }

        public abstract void ShowState();
        
    }
    
    class ConcreteStateA : State
    {
        public override void ShowState()
        {
            time = 0;
        }
        
    }
    class ConcreteStateB : State
    {
        public override void ShowState()
        {
            time = 4;
        }        
    }
    class ConcreteStateC : State
    {
        public override void ShowState()
        {
            time = 8;
        }        
    }







    //сейвим інфу про кіькість ресурсів і ще недосліджених частин карти

    //!!!!!!ЗРОБИТИ СЕРІАЛІЗАЦІЮ!!!!!!!!!

    class info
    {
       public int unDiscovered=16;
       public int gold=0;
       public int iron=0;
       public int coal=0;
    }

    class Originator
    {
        private string _state;        
        public string getSaved()
        {
            return _state;
        }
        
        public Memento CreateMemento(info current)
        {
            _state = JsonConvert.SerializeObject(current);
            return (new Memento(_state));
        }



        public void SetMemento(Memento memento)
        {
            State = memento.State;
        }

        public string State
        {
            get { return _state; }
            set
            {
                _state = value;
            }
        }
    }
    
    class Memento
    {
        private string _state;
       
        public Memento(string state)
        {
            this._state = state;
        }
        public string State
        {
            get { return _state; }
        }
    }
    












    //структурні шаблони


    //слідкує за легковаговиком і декоратором
    

    public class Facade
    {
        protected FlyWeightFactory flyWeight;
        protected Creator creator;
        public Facade(FlyWeightFactory subsystem1, Creator subsystem2)
        {
            this.flyWeight = subsystem1;
            this.creator = subsystem2;
        }        
        public string Operations()
        {
            string result = "\nFacade initializes subsystems:\n";
            result += this.flyWeight.isDone();
            result += this.creator.isCreated();
            return result;
        }
    }






    //для ресурсів, коли наприклад два золота підряд попались з різною тільки кількістю ресурсу в клітинці

    public class FlyWeightFactory
    {
        private Hashtable characters = new Hashtable();
        bool atLeastOne = false;
        public FlyWeight GetCharacter(string key)
        {
            FlyWeight character = characters[key] as FlyWeight;
            if (character == null)
            {
                switch (key)
                {
                    case "Gold": character = new fwGold(); break;
                    case "Coal": character = new fwCoal(); break;
                    case "Iron": character = new fwIron(); break;
                }
                characters.Add(key, character);
                atLeastOne = true;
            }
            return character;
        }

        public string isDone()
        {
            if(atLeastOne)
            {
                return "FlyWeightFactory is working";
            }
            else
            {
                return "FlyWeightFactory is not working";
            }
        }
    }
    abstract public class FlyWeight
    {
        protected string name;//внутрішній
        protected double price;//внутрішній
        protected double speedOfMining;//внутрішній
        protected double volume;//зовнішній
        public virtual void setVolume(double volume)
        {
            this.volume = volume;
        }
        public virtual double getVolume()
        {
            return this.volume;
        }
        public virtual string getInfo()
        {
            string info = this.name + "\nPrice: " + this.price + "\nSpeed of mining it: " + this.speedOfMining;
            return info;
        }
    }
    class fwGold : FlyWeight
    {
        public fwGold()
        {
            this.name = " Gold ";
            this.price = 100.4;
            this.speedOfMining = 120;
        }
    }    
    class fwCoal : FlyWeight
    {
        public fwCoal()
        {
            this.name = " Coal ";
            this.price = 70;
            this.speedOfMining = 60;
        }
    }    
    class fwIron : FlyWeight
    {
        public fwIron()
        {
            this.name = " Iron ";
            this.price = 87;
            this.speedOfMining = 88.8;
        }
    }







    //мішаєм метали і получаєм сплав


    public abstract class AlloyBase
    {
        public int Price { get; protected set; }        
        public AlloyBase()
        {
        }
    }
    public class Iron : AlloyBase
    {
        public Iron() 
        {
            Price += 20;
        }
    }
    public class Cooper : AlloyBase
    {
        public Cooper()
        {
            Price += 10;
        }
    }
    public class Tin : AlloyBase
    {
        public Tin()
        {
            Price += 15;
        }
    }
    public abstract class AlloyAdditiveBase : AlloyBase
    {
        public AlloyAdditiveBase()
        {
        }
    }
    public class CarbonAdditive : AlloyAdditiveBase
    {
        public CarbonAdditive()
        {
            Price += 10;
        }
    }
    public class NikelAdditive : AlloyAdditiveBase
    {
        public NikelAdditive()
        {
            Price += 20;
        }
    }
    public class GoldAdditive : AlloyAdditiveBase
    {
         public GoldAdditive()
        {
            Price += 50;
        }
    }
    public class ZinkAdditive : AlloyAdditiveBase
    {
        public ZinkAdditive()
        {
            Price += 15;
        }
    }
   public class Creator
    {
        string name = "";
        public string info="";
        bool firstAdditive = true;
        public int price = 0;
        bool isUsed = false;
        public void createAlloy(string temp, string type)
        {
            price = 0;
            name = temp;
            info = "You have created new alloy " + name + " with base metal "+ type;
            if(type == "Iron")
            {
                AlloyBase alloy = new Iron();
                price += alloy.Price;
            }
            if (type == "Cooper")
            {
                AlloyBase alloy = new Cooper();
                price += alloy.Price;
            }
            if (type == "Tin")
            {
                AlloyBase alloy = new Tin();
                price += alloy.Price;
            }
            info += " ";
            firstAdditive = true;
        }
        public void addAdditives(string type)
        {
            if(firstAdditive)
            {
                info += "and other additives such as ";
                firstAdditive = false;
            }
            info += type;
            AlloyAdditiveBase addative;
            if (type == "Carbon")
            {
                addative= new CarbonAdditive();
                price += addative.Price;
            }
            if (type == "Nikel")
            {
                addative = new NikelAdditive();
                price += addative.Price;
            }
            if (type == "Gold")
            {
                addative = new GoldAdditive();
                price += addative.Price;
            }
            if (type == "Zink")
            {
                addative = new ZinkAdditive();
                price += addative.Price;
            }
            info += " ";
            isUsed = true;
        }
        public string isCreated()
        {
            if(isUsed)
            {
                return "\nDecorator was already used";
            }
            else
            {
                return "\nDecorator isn't used yet";
            }
        }
    }
          



    //твірні шаблони


    //для ресурсів)0)))


    public class Recource
    {
        public double amount;
        public string name;
        public double price;

        public Recource Copy()
        {
            Recource clone = (Recource)this.MemberwiseClone();
            clone.name = String.Copy(name);
            return clone;
        }
    }                  

    public interface IAbstractFactory//наш інтерфейс з методами створення абстрактних продуктів
    {
        IAbstractProductA CreateProductA();

        IAbstractProductB CreateProductB();
    }

    //метод з інтерфейсу, котрий повертає вміст продукту
    public interface IAbstractProductA
    {
        string WhatContainA();
    }

    //вміст продукту
    internal class ConcreteProductA1 : IAbstractProductA
    {
        public string WhatContainA()
        {
            return " mine";
        }
    }

    //вміст продукту
    internal class ConcreteProductA2 : IAbstractProductA
    {
        public string WhatContainA()
        {
            return " factory";
        }
    }

    //те саме тільки для б
    public interface IAbstractProductB
    {
        string WhatContainB_A(IAbstractProductA collaborator);
    }

    internal class ConcreteProductB1 : IAbstractProductB
    {

        public string WhatContainB_A(IAbstractProductA collaborator)
        {
            var result = collaborator.WhatContainA();
            return $"Simple {result}";
        }
    }

    internal class ConcreteProductB2 : IAbstractProductB
    {

        public string WhatContainB_A(IAbstractProductA collaborator)
        {
            var result = collaborator.WhatContainA();
            return $"Modern {result}";
        }
    }

    //фабрики для комбінування двох абстрактних продуктів в один конкретний
    internal class ConcreteFactory1 : IAbstractFactory
    {
        public IAbstractProductA CreateProductA()
        {
            return new ConcreteProductA1();
        }

        public IAbstractProductB CreateProductB()
        {
            return new ConcreteProductB1();
        }
    }

    internal class ConcreteFactory2 : IAbstractFactory
    {
        public IAbstractProductA CreateProductA()
        {
            return new ConcreteProductA2();
        }

        public IAbstractProductB CreateProductB()
        {
            return new ConcreteProductB2();
        }
    }

    internal class ConcreteFactory3 : IAbstractFactory
    {
        public IAbstractProductA CreateProductA()
        {
            return new ConcreteProductA2();
        }

        public IAbstractProductB CreateProductB()
        {
            return new ConcreteProductB1();
        }
    }

    //клієнт
    internal class FactoryClient
    {
        public string info;
        public int count1 = 0;
        public int count2 = 0;
        public int count3 = 0;
        public void S_m()
        {
            ClientMethod(new ConcreteFactory1());
            count1++;
        }
        public void M_f()
        {
            ClientMethod(new ConcreteFactory2());
            count2++;
        }
        public void S_f()
        {
            ClientMethod(new ConcreteFactory3());
            count3++;
        }
        public void ClientMethod(IAbstractFactory factory)
        {
            var productA = factory.CreateProductA();
            var productB = factory.CreateProductB();            
            info = productB.WhatContainB_A(productA);
        }
    }
    

    public class LazySingleton
    {
        private static int time = 0;
        private static LazySingleton instance;
        public static LazySingleton Instance()
        {
            return instance ?? (instance = new LazySingleton());
        }
        protected LazySingleton(){}
        public void set_t(int n)
        {
            time = n;
        }
        public int get_t()
        {
            return time;
        }
    }

}


public class Singleton
{
    public static int played;
    public static int level;
    public static string name;
    private Singleton() { }

    private static Singleton _instance;
    
    private static readonly object _lock = new object();

    public static Singleton GetInstance(int p, int l, string n)
    {
        if (_instance == null)
        {
            lock (_lock)
            {
                if (_instance == null)
                {
                    _instance = new Singleton();
                    played = p;
                    level = l;
                    name = n;
                }
            }
        }
        return _instance;
    }
}
