﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Game_0._1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            Disasters disasters = new Disasters();
            Money money = new Money();
            Achievments achievements = new Achievments();
            List<ICommand> commands = new List<ICommand>
            {
                new DisasterCommand(disasters, "F"),
                new MoneyCommand(money, "M"),
                new MoneyCommand(money, "M"),
                new MoneyCommand(money, "M"),
                new AchieveCommand(achievements,"EXP"),
                new AchieveCommand(achievements,"GD")
            };
            Manager manager = new Manager();
            manager.SetCommand(new MacroCommand(commands));
            manager.DoProject();
            richTextBox1.Text = richTextBox1.Text + manager.info;
        }

        Subject sub1 = new Subject();
        Observer obs1;

        private void button6_Click(object sender, EventArgs e)
        {
            obs1 = new Observer("TradeMarket", sub1);
            if (checkBox3.Checked)
            {
                sub1.increaseCoeff(0.1);
                richTextBox1.Text = richTextBox1.Text + "\nYour trade coeff is: " + sub1.getCoeff();
                obs1.SaveSubject(sub1);
                richTextBox1.Text = richTextBox1.Text + "\nChanges applied a\n";
                richTextBox1.Text = richTextBox1.Text + "\nSo you can sell with price: " + obs1.Sell(sub1.getCoeff());
            }
            if (checkBox2.Checked)
            {
                sub1.decreaseCoeff(0.1);
                richTextBox1.Text = richTextBox1.Text + "\nYour trade coeff is: " + sub1.getCoeff();
                obs1.SaveSubject(sub1);
                richTextBox1.Text = richTextBox1.Text + "\nChanges applied at\n";
                richTextBox1.Text = richTextBox1.Text + "\nSo you can sell with price: " + obs1.Sell(sub1.getCoeff());
            }
        }
    }





    //поведінкові шаблони2

    //Або шось аля консолькі з чітами, або шось інше придумати

    interface ICommand
    {
        string Do();//робить
        string Undo();//вертає
    }
    class MacroCommand : ICommand
    {
        string Info = "";
        List<ICommand> commands;
        public MacroCommand(List<ICommand> coms)
        {
            commands = coms;//список
        }
        public string Do()
        {
            foreach (ICommand c in commands)
                Info += c.Do();
            return Info;
        }

        public string Undo()
        {
            foreach (ICommand c in commands)
                Info += c.Undo();
            return Info;
        }
    }

    class Achievments
    {
        public string GoldDigger(bool _do)
        {
            if (_do)
            {
                return "\nYou have found a lot of gold? so you are a Gold Digger now!\n";
            }
            else
            {
                return "\nThe achievement was removed\n";
            }
        }
        public string Explorer(bool _do)
        {
            if (_do)
            {
                return "\nYou have explored a lot of maps, we give you achevement Explorer!\n";
            }
            else
            {
                return "\nThe achievement was removed\n";
            }
        }
    }

    class Disasters
    {
        public string EarthShake(bool _do)
        {
            if (_do)
            {
                return "\nErthshake has changed recources location\n";
            }
            else
            {
                return "\nGod came to us and repaired everything\n";
            }
        }
        public string Fire(bool _do)
        {
            if (_do)
            {
                return "\nOne of your buildings is on fire now!\n";
            }
            else
            {
                return "\nLuckily, fire didn't destrouy you building\n";
            }
        }
    }

    class Money
    {
        public string More(bool _do)
        {
            if (_do)
            {
                return "\n+500 coins\n";
            }
            else
            {
                return "\n-500 coins\n";
            }
        }
        public string Less(bool _do)
        {
            if (_do)
            {
                return "\n-500 coins\n";
            }
            else
            {
                return "\n+500 coins\n";
            }
        }
    }

    class AchieveCommand : ICommand
    {
        Achievments achievment;
        string check;
        string info = "";
        public AchieveCommand(Achievments a, string temp)
        {
            achievment = a;
            check = temp;
        }
        public string Do()
        {
            if (check == "GD")
            {
                info += achievment.GoldDigger(true);
            }
            if (check == "EXP")
            {
                info += achievment.Explorer(true);
            }
            return info;
        }
        public string Undo()
        {
            if (check == "GD")
            {
                info += achievment.GoldDigger(false);
            }
            if (check == "EXP")
            {
                info += achievment.Explorer(false);
            }
            return info;
        }
    }

    class DisasterCommand : ICommand
    {
        Disasters disaster;
        string check;
        string info;

        public DisasterCommand(Disasters d, string temp)
        {
            disaster = d;
            check = temp;
        }
        public string Do()
        {
            if (check == "ESH")
            {
                info += disaster.EarthShake(true);
            }
            if (check == "F")
            {
                info += disaster.Fire(true);
            }
            return info;
        }
        public string Undo()
        {
            if (check == "ESH")
            {
                info += disaster.EarthShake(false);
            }
            if (check == "F")
            {
                info += disaster.Fire(false);
            }
            return info;
        }
    }

    class MoneyCommand : ICommand
    {
        Money money;
        string check;
        string info;
        public MoneyCommand(Money m, string temp)
        {
            money = m;
            check = temp;
        }
        public string Do()
        {
            if (check == "M")
            {
                info += money.More(true);
            }
            if (check == "L")
            {
                info += money.Less(true);
            }
            return info;
        }
        public string Undo()
        {
            if (check == "M")
            {
                info += money.More(false);
            }
            if (check == "L")
            {
                info += money.Less(false);
            }
            return info;
        }
    }

    class Manager
    {
        public string info = "";
        ICommand command;
        public void SetCommand(ICommand com)
        {
            command = com;
        }
        public void DoProject()
        {
            if (command != null)
                info += command.Do();
        }
        public void UndoProject()
        {

            if (command != null)
                info += command.Undo();
        }
    }


    //апдейт клоли змінюється коефіцієнт торгівлі


    class Observer
    {
        Subject sState;
        double sell_price = 5.78;
        public string Title { get; set; }
        public Observer(string title, Subject subject)
        {
            if (string.IsNullOrEmpty(title))
            {
                throw new ArgumentNullException(nameof(title));
            }
            if (subject == null)
            {
                throw new ArgumentNullException(nameof(subject));
            }
            Title = title;
            sState = subject;
        }
        public bool SaveSubject(Subject subject)
        {
            if (sState != subject)
            {
                sState = subject;
                return true;
            }
            else
            {
                return false;
            }
        }
        public double Sell(double coeff)
        {
            double price = sell_price * coeff;
            return price;
        }
    }

    class Subject
    {
        private double sell_coeff = 0.75;
        public void setCoeff(double temp)
        {
            sell_coeff = temp;
        }

        public void increaseCoeff(double temp)
        {
            sell_coeff += temp;
        }

        public void decreaseCoeff(double temp)
        {
            sell_coeff -= temp;
        }

        public double getCoeff()
        {
            return sell_coeff;
        }
    }
}
