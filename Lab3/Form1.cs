﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Collections;
using Newtonsoft.Json;

namespace Game_0._1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            pictureBox1.Image = Properties.Resources.map;
            pictureBox2.Image = Properties.Resources.rec_map;
            pictureBox3.Image = Properties.Resources.rec_map;
            pictureBox4.Image = Properties.Resources.rec_map;
            pictureBox5.Image = Properties.Resources.rec_map;
            pictureBox6.Image = Properties.Resources.rec_map;
            pictureBox7.Image = Properties.Resources.rec_map;
            pictureBox8.Image = Properties.Resources.rec_map;
            pictureBox9.Image = Properties.Resources.rec_map;
            pictureBox10.Image = Properties.Resources.rec_map;
            pictureBox11.Image = Properties.Resources.rec_map;
            pictureBox12.Image = Properties.Resources.rec_map;
            pictureBox13.Image = Properties.Resources.rec_map;
            pictureBox14.Image = Properties.Resources.rec_map;
            pictureBox15.Image = Properties.Resources.rec_map;
            pictureBox16.Image = Properties.Resources.rec_map;
            pictureBox17.Image = Properties.Resources.rec_map;

        }

        FlyWeightFactory fw = new FlyWeightFactory();
        private void button1_Click(object sender, EventArgs e)
        {
        }
        Creator client = new Creator();
        private void button1_Click_1(object sender, EventArgs e)
        {
            richTextBox1.Text = richTextBox1.Text + "\nDecorator: \n";
            client.createAlloy("Alloy1", "Iron");
            client.addAdditives("Gold");
            client.addAdditives("Zink");
            string alloy1 = client.info;
            richTextBox1.Text = richTextBox1.Text + alloy1 + " and its price is " + client.price + "\n";
            client.createAlloy("Alloy2", "Tin");
            client.addAdditives("Nikel");
            string alloy2 = client.info;
            richTextBox1.Text = richTextBox1.Text + alloy2 + " and its price is " + client.price + "\n";

        }

        private void pictureBox16_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = richTextBox1.Text + "\nYou have found:\n";
            pictureBox16.Image = Properties.Resources.iron;
            FlyWeight iron = fw.GetCharacter("Iron");
            iron.setVolume(15);
            double temp;
            temp = iron.getVolume();
            string info = iron.getInfo();
            richTextBox1.Text = richTextBox1.Text + info + "\nVolume:" + temp;
        }

        private void pictureBox14_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = richTextBox1.Text + "\nYou have found:\n";
            pictureBox14.Image = Properties.Resources.coal;
            FlyWeight coal = fw.GetCharacter("Coal");
            coal.setVolume(10);
            double temp;
            temp = coal.getVolume();
            string info = coal.getInfo();
            richTextBox1.Text = richTextBox1.Text + info + "\nVolume:" + temp;
        }

        private void pictureBox10_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = richTextBox1.Text + "\nYou have found:\n";
            pictureBox10.Image = Properties.Resources.coal;
            FlyWeight coal = fw.GetCharacter("Coal");
            coal.setVolume(12);
            double temp;
            temp = coal.getVolume();
            string info = coal.getInfo();
            richTextBox1.Text = richTextBox1.Text + info + "\nVolume:" + temp;
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = richTextBox1.Text + "\nYou have found:\n";
            pictureBox6.Image = Properties.Resources.coal;
            FlyWeight coal = fw.GetCharacter("Coal");
            coal.setVolume(14);
            double temp;
            temp = coal.getVolume();
            string info = coal.getInfo();
            richTextBox1.Text = richTextBox1.Text + info + "\nVolume:" + temp;
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = richTextBox1.Text + "\nFlyWeight: \n";
            pictureBox4.Image = Properties.Resources.gold;
            FlyWeight gold = fw.GetCharacter("Gold");
            gold.setVolume(50);
            double temp;
            temp = gold.getVolume();
            string info = gold.getInfo();
            richTextBox1.Text = richTextBox1.Text + info + "\nVolume:" + temp;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Facade facade = new Facade(fw, client);
            string check = facade.Operations();
            richTextBox1.Text = richTextBox1.Text + check;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }


    //структурні шаблони


    //слідкує за легковаговиком і декоратором


    public class Facade
    {
        protected FlyWeightFactory flyWeight;
        protected Creator creator;
        public Facade(FlyWeightFactory subsystem1, Creator subsystem2)
        {
            this.flyWeight = subsystem1;
            this.creator = subsystem2;
        }
        public string Operations()
        {
            string result = "\nFacade initializes subsystems:\n";
            result += this.flyWeight.isDone();
            result += this.creator.isCreated();
            return result;
        }
    }






    //для ресурсів, коли наприклад два золота підряд попались з різною тільки кількістю ресурсу в клітинці

    public class FlyWeightFactory
    {
        private Hashtable characters = new Hashtable();
        bool atLeastOne = false;
        public FlyWeight GetCharacter(string key)
        {
            FlyWeight character = characters[key] as FlyWeight;
            if (character == null)
            {
                switch (key)
                {
                    case "Gold": character = new fwGold(); break;
                    case "Coal": character = new fwCoal(); break;
                    case "Iron": character = new fwIron(); break;
                }
                characters.Add(key, character);
                atLeastOne = true;
            }
            return character;
        }

        public string isDone()
        {
            if (atLeastOne)
            {
                return "FlyWeightFactory is working";
            }
            else
            {
                return "FlyWeightFactory is not working";
            }
        }
    }
    abstract public class FlyWeight
    {
        protected string name;//внутрішній
        protected double price;//внутрішній
        protected double speedOfMining;//внутрішній
        protected double volume;//зовнішній
        public virtual void setVolume(double volume)
        {
            this.volume = volume;
        }
        public virtual double getVolume()
        {
            return this.volume;
        }
        public virtual string getInfo()
        {
            string info = this.name + "\nPrice: " + this.price + "\nSpeed of mining it: " + this.speedOfMining;
            return info;
        }
    }
    class fwGold : FlyWeight
    {
        public fwGold()
        {
            this.name = " Gold ";
            this.price = 100.4;
            this.speedOfMining = 120;
        }
    }
    class fwCoal : FlyWeight
    {
        public fwCoal()
        {
            this.name = " Coal ";
            this.price = 70;
            this.speedOfMining = 60;
        }
    }
    class fwIron : FlyWeight
    {
        public fwIron()
        {
            this.name = " Iron ";
            this.price = 87;
            this.speedOfMining = 88.8;
        }
    }







    //мішаєм метали і получаєм сплав


    public abstract class AlloyBase
    {
        public int Price { get; protected set; }
        public AlloyBase()
        {
        }
    }
    public class Iron : AlloyBase
    {
        public Iron()
        {
            Price += 20;
        }
    }
    public class Cooper : AlloyBase
    {
        public Cooper()
        {
            Price += 10;
        }
    }
    public class Tin : AlloyBase
    {
        public Tin()
        {
            Price += 15;
        }
    }
    public abstract class AlloyAdditiveBase : AlloyBase
    {
        public AlloyAdditiveBase()
        {
        }
    }
    public class CarbonAdditive : AlloyAdditiveBase
    {
        public CarbonAdditive()
        {
            Price += 10;
        }
    }
    public class NikelAdditive : AlloyAdditiveBase
    {
        public NikelAdditive()
        {
            Price += 20;
        }
    }
    public class GoldAdditive : AlloyAdditiveBase
    {
        public GoldAdditive()
        {
            Price += 50;
        }
    }
    public class ZinkAdditive : AlloyAdditiveBase
    {
        public ZinkAdditive()
        {
            Price += 15;
        }
    }
    public class Creator
    {
        string name = "";
        public string info = "";
        bool firstAdditive = true;
        public int price = 0;
        bool isUsed = false;
        public void createAlloy(string temp, string type)
        {
            price = 0;
            name = temp;
            info = "You have created new alloy " + name + " with base metal " + type;
            if (type == "Iron")
            {
                AlloyBase alloy = new Iron();
                price += alloy.Price;
            }
            if (type == "Cooper")
            {
                AlloyBase alloy = new Cooper();
                price += alloy.Price;
            }
            if (type == "Tin")
            {
                AlloyBase alloy = new Tin();
                price += alloy.Price;
            }
            info += " ";
            firstAdditive = true;
        }
        public void addAdditives(string type)
        {
            if (firstAdditive)
            {
                info += "and other additives such as ";
                firstAdditive = false;
            }
            info += type;
            AlloyAdditiveBase addative;
            if (type == "Carbon")
            {
                addative = new CarbonAdditive();
                price += addative.Price;
            }
            if (type == "Nikel")
            {
                addative = new NikelAdditive();
                price += addative.Price;
            }
            if (type == "Gold")
            {
                addative = new GoldAdditive();
                price += addative.Price;
            }
            if (type == "Zink")
            {
                addative = new ZinkAdditive();
                price += addative.Price;
            }
            info += " ";
            isUsed = true;
        }
        public string isCreated()
        {
            if (isUsed)
            {
                return "\nDecorator was already used";
            }
            else
            {
                return "\nDecorator isn't used yet";
            }
        }
    }


}
