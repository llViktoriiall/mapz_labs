﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Collections;



namespace Game_0._1
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
            pictureBox1.Image = Properties.Resources.map;
            pictureBox2.Image = Properties.Resources.rec_map;
            pictureBox3.Image = Properties.Resources.rec_map;
            pictureBox4.Image = Properties.Resources.rec_map;
            pictureBox5.Image = Properties.Resources.rec_map;
            pictureBox6.Image = Properties.Resources.rec_map;
            pictureBox7.Image = Properties.Resources.rec_map;
            pictureBox8.Image = Properties.Resources.rec_map;
            pictureBox9.Image = Properties.Resources.rec_map;
            pictureBox10.Image = Properties.Resources.rec_map;
            pictureBox11.Image = Properties.Resources.rec_map;
            pictureBox12.Image = Properties.Resources.rec_map;
            pictureBox13.Image = Properties.Resources.rec_map;
            pictureBox14.Image = Properties.Resources.rec_map;
            pictureBox15.Image = Properties.Resources.rec_map;
            pictureBox16.Image = Properties.Resources.rec_map;
            pictureBox17.Image = Properties.Resources.rec_map;
        }

        FlyWeightFactory fw = new FlyWeightFactory();
        Creator client = new Creator();
        private void button1_Click(object sender, EventArgs e)
        {
            string info = "";
            Thread process1 = new Thread(() =>
            {
                Singleton.GetInstance(6,4,"Ivan2008");
                info = "Hello, " + Singleton.name + ". You are at level " + Singleton.level + " and you already have played " + Singleton.played + " games.\n";
            });
            Thread process2 = new Thread(() =>
            {
               Singleton.GetInstance(7,7,"SuperPuperOliia");
                info = "Hello, " + Singleton.name + ". You are at level " + Singleton.level + " and you already have played " + Singleton.played + " games.\n";
            });
            richTextBox1.Text = "Thread-safe singleton:\n";
            process1.Start();
            process2.Start();
            process1.Join();
            richTextBox1.Text = richTextBox1.Text + "The singleton after first thread: \n";
            richTextBox1.Text = richTextBox1.Text + info;
            process2.Join();
            richTextBox1.Text = richTextBox1.Text + "The singleton after second thread: \n";
            richTextBox1.Text = richTextBox1.Text + info;
                    
            


            richTextBox1.Text = richTextBox1.Text + "\nLazy singleton: \n";
            LazySingleton s1 = LazySingleton.Instance();
            LazySingleton s2 = LazySingleton.Instance();
            s1.set_t(30);
            int time = s2.get_t();
            richTextBox1.Text = richTextBox1.Text  + "You have " + time + " minutes to complete the round\n";
                      
            


            richTextBox1.Text = richTextBox1.Text + "\nAbstract fabric: \n";
            FactoryClient s = new FactoryClient();
            s.M_f();
            string t = s.info;
            richTextBox1.Text = richTextBox1.Text + "You have " + s.count2+" "+ t +"\n";
            s.S_m();
            t = s.info;
            richTextBox1.Text = richTextBox1.Text + "You have " + s.count1+" "+ t+"\n";
            s.S_f();
            s.S_f();
            t = s.info;
            richTextBox1.Text = richTextBox1.Text + "You have " + s.count3 + " " + t+"\n";




            richTextBox1.Text = richTextBox1.Text + "\nPrototype: \nOn this level you have next recources: \n";
            Recource r1 = new Recource();
            r1.name = "Oil";
            r1.amount = 0.55;
            r1.price = 6.2;
            string inf = r1.name + " in amount of " + r1.amount + " with price: " + r1.price+"\n";
            richTextBox1.Text = richTextBox1.Text + inf;
            Recource r2 = r1.Copy();
            Recource r3 = r1.Copy();
            r2.name = "Gold";
            inf = r2.name + " in amount of " + r2.amount + " with price: " + r2.price + "\n";
            richTextBox1.Text = richTextBox1.Text + inf;
            r3.price = 0.77;
            inf = r3.name + " in amount of " + r3.amount + " with price: " + r3.price + "\n";
            richTextBox1.Text = richTextBox1.Text + inf;




            richTextBox1.Text = richTextBox1.Text + "\nDecorator: \n";
            client.createAlloy("Alloy1", "Iron");
            client.addAdditives("Gold");
            client.addAdditives("Zink");
            string alloy1 = client.info;
            richTextBox1.Text = richTextBox1.Text + alloy1 + " and its price is "+client.price+"\n";
            client.createAlloy("Alloy2", "Tin");
            client.addAdditives("Nikel");
            string alloy2 = client.info;
            richTextBox1.Text = richTextBox1.Text + alloy2 + " and its price is " + client.price+"\n";
        }
        
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = richTextBox1.Text + "OK";
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = richTextBox1.Text + "\nFlyWeight: \nYou have found:\n";
            pictureBox4.Image = Properties.Resources.gold;
            FlyWeight gold = fw.GetCharacter("Gold");
            gold.setVolume(20);
            double temp;
            temp = gold.getVolume();
            string info = gold.getInfo();
            richTextBox1.Text = richTextBox1.Text + info + "\nVolume:" + temp;
        }

        private void pictureBox14_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = richTextBox1.Text + "\nFlyWeight: \nYou have found:\n";
            pictureBox14.Image = Properties.Resources.coal;
            FlyWeight coal = fw.GetCharacter("Coal");
            coal.setVolume(10);
            double temp;
            temp = coal.getVolume();
            string info = coal.getInfo();
            richTextBox1.Text = richTextBox1.Text + info + "\nVolume:" + temp;
        }

        private void pictureBox16_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = richTextBox1.Text + "\nFlyWeight: \nYou have found:\n";
            pictureBox16.Image = Properties.Resources.iron;
            FlyWeight iron = fw.GetCharacter("Iron");
            iron.setVolume(15);
            double temp;
            temp = iron.getVolume();
            string info = iron.getInfo();
            richTextBox1.Text = richTextBox1.Text + info + "\nVolume:" + temp;
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = richTextBox1.Text + "\nFlyWeight: \n";
            pictureBox5.Image = Properties.Resources.gold;
            FlyWeight gold = fw.GetCharacter("Gold");
            gold.setVolume(50);
            double temp;
            temp = gold.getVolume();
            string info = gold.getInfo();
            richTextBox1.Text = richTextBox1.Text + info + "\nVolume:" + temp;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Facade facade = new Facade(fw, client);
            string check = facade.Operations();
            richTextBox1.Text = richTextBox1.Text + check;
        }
    }



    //структурні шаблони

    public class Facade
    {
        protected FlyWeightFactory flyWeight;
        protected Creator creator;
        public Facade(FlyWeightFactory subsystem1, Creator subsystem2)
        {
            this.flyWeight = subsystem1;
            this.creator = subsystem2;
        }        
        public string Operations()
        {
            string result = "\nFacade initializes subsystems:\n";
            result += this.flyWeight.isDone();
            result += this.creator.isCreated();
            return result;
        }
    }



    


    public class FlyWeightFactory
    {
        private Hashtable characters = new Hashtable();
        bool atLeastOne = false;
        public FlyWeight GetCharacter(string key)
        {
            FlyWeight character = characters[key] as FlyWeight;
            if (character == null)
            {
                switch (key)
                {
                    case "Gold": character = new fwGold(); break;
                    case "Coal": character = new fwCoal(); break;
                    case "Iron": character = new fwIron(); break;
                }
                characters.Add(key, character);
                atLeastOne = true;
            }
            return character;
        }

        public string isDone()
        {
            if(atLeastOne)
            {
                return "FlyWeightFactory is working";
            }
            else
            {
                return "FlyWeightFactory is not working";
            }
        }
    }
    abstract public class FlyWeight
    {
        protected string name;//внутрішній
        protected double price;//внутрішній
        protected double speedOfMining;//внутрішній
        protected double volume;//зовнішній
        public virtual void setVolume(double volume)
        {
            this.volume = volume;
        }
        public virtual double getVolume()
        {
            return this.volume;
        }
        public virtual string getInfo()
        {
            string info = this.name + "\nPrice: " + this.price + "\nSpeed of mining it: " + this.speedOfMining;
            return info;
        }
    }
    class fwGold : FlyWeight
    {
        public fwGold()
        {
            this.name = " Gold ";
            this.price = 100.4;
            this.speedOfMining = 120;
        }
    }    
    class fwCoal : FlyWeight
    {
        public fwCoal()
        {
            this.name = " Coal ";
            this.price = 70;
            this.speedOfMining = 60;
        }
    }    
    class fwIron : FlyWeight
    {
        public fwIron()
        {
            this.name = " Iron ";
            this.price = 87;
            this.speedOfMining = 88.8;
        }
    }










    public abstract class AlloyBase
    {
        public int Price { get; protected set; }        
        public AlloyBase()
        {
        }
    }
    public class Iron : AlloyBase
    {
        public Iron() 
        {
            Price += 20;
        }
    }
    public class Cooper : AlloyBase
    {
        public Cooper()
        {
            Price += 10;
        }
    }
    public class Tin : AlloyBase
    {
        public Tin()
        {
            Price += 15;
        }
    }
    public abstract class AlloyAdditiveBase : AlloyBase
    {
        public AlloyAdditiveBase()
        {
        }
    }
    public class CarbonAdditive : AlloyAdditiveBase
    {
        public CarbonAdditive()
        {
            Price += 10;
        }
    }
    public class NikelAdditive : AlloyAdditiveBase
    {
        public NikelAdditive()
        {
            Price += 20;
        }
    }
    public class GoldAdditive : AlloyAdditiveBase
    {
         public GoldAdditive()
        {
            Price += 50;
        }
    }
    public class ZinkAdditive : AlloyAdditiveBase
    {
        public ZinkAdditive()
        {
            Price += 15;
        }
    }
   public class Creator
    {
        string name = "";
        public string info="";
        bool firstAdditive = true;
        public int price = 0;
        bool isUsed = false;
        public void createAlloy(string temp, string type)
        {
            price = 0;
            name = temp;
            info = "You have created new alloy " + name + " with base metal "+ type;
            if(type == "Iron")
            {
                AlloyBase alloy = new Iron();
                price += alloy.Price;
            }
            if (type == "Cooper")
            {
                AlloyBase alloy = new Cooper();
                price += alloy.Price;
            }
            if (type == "Tin")
            {
                AlloyBase alloy = new Tin();
                price += alloy.Price;
            }
            info += " ";
            firstAdditive = true;
        }
        public void addAdditives(string type)
        {
            if(firstAdditive)
            {
                info += "and other additives such as ";
                firstAdditive = false;
            }
            info += type;
            AlloyAdditiveBase addative;
            if (type == "Carbon")
            {
                addative= new CarbonAdditive();
                price += addative.Price;
            }
            if (type == "Nikel")
            {
                addative = new NikelAdditive();
                price += addative.Price;
            }
            if (type == "Gold")
            {
                addative = new GoldAdditive();
                price += addative.Price;
            }
            if (type == "Zink")
            {
                addative = new ZinkAdditive();
                price += addative.Price;
            }
            info += " ";
            isUsed = true;
        }
        public string isCreated()
        {
            if(isUsed)
            {
                return "\nDecorator was already used";
            }
            else
            {
                return "\nDecorator isn't used yet";
            }
        }
    }














    //твірні шаблони

    public class Recource
    {
        public double amount;
        public string name;
        public double price;

        public Recource Copy()
        {
            Recource clone = (Recource)this.MemberwiseClone();
            clone.name = String.Copy(name);
            return clone;
        }
    }                  

    public interface IAbstractFactory//наш інтерфейс з методами створення абстрактних продуктів
    {
        IAbstractProductA CreateProductA();

        IAbstractProductB CreateProductB();
    }

    //метод з інтерфейсу, котрий повертає вміст продукту
    public interface IAbstractProductA
    {
        string WhatContainA();
    }

    //вміст продукту
    internal class ConcreteProductA1 : IAbstractProductA
    {
        public string WhatContainA()
        {
            return " mine";
        }
    }

    //вміст продукту
    internal class ConcreteProductA2 : IAbstractProductA
    {
        public string WhatContainA()
        {
            return " factory";
        }
    }

    //те саме тільки для б
    public interface IAbstractProductB
    {
        string WhatContainB_A(IAbstractProductA collaborator);
    }

    internal class ConcreteProductB1 : IAbstractProductB
    {

        public string WhatContainB_A(IAbstractProductA collaborator)
        {
            var result = collaborator.WhatContainA();
            return $"Simple {result}";
        }
    }

    internal class ConcreteProductB2 : IAbstractProductB
    {

        public string WhatContainB_A(IAbstractProductA collaborator)
        {
            var result = collaborator.WhatContainA();
            return $"Modern {result}";
        }
    }

    //фабрики для комбінування двох абстрактних продуктів в один конкретний
    internal class ConcreteFactory1 : IAbstractFactory
    {
        public IAbstractProductA CreateProductA()
        {
            return new ConcreteProductA1();
        }

        public IAbstractProductB CreateProductB()
        {
            return new ConcreteProductB1();
        }
    }

    internal class ConcreteFactory2 : IAbstractFactory
    {
        public IAbstractProductA CreateProductA()
        {
            return new ConcreteProductA2();
        }

        public IAbstractProductB CreateProductB()
        {
            return new ConcreteProductB2();
        }
    }

    internal class ConcreteFactory3 : IAbstractFactory
    {
        public IAbstractProductA CreateProductA()
        {
            return new ConcreteProductA2();
        }

        public IAbstractProductB CreateProductB()
        {
            return new ConcreteProductB1();
        }
    }

    //клієнт
    internal class FactoryClient
    {
        public string info;
        public int count1 = 0;
        public int count2 = 0;
        public int count3 = 0;
        public void S_m()
        {
            ClientMethod(new ConcreteFactory1());
            count1++;
        }
        public void M_f()
        {
            ClientMethod(new ConcreteFactory2());
            count2++;
        }
        public void S_f()
        {
            ClientMethod(new ConcreteFactory3());
            count3++;
        }
        public void ClientMethod(IAbstractFactory factory)
        {
            var productA = factory.CreateProductA();
            var productB = factory.CreateProductB();            
            info = productB.WhatContainB_A(productA);
        }
    }
          

    public class LazySingleton
    {
        private static int time = 0;
        private static LazySingleton instance;
        public static LazySingleton Instance()
        {
            return instance ?? (instance = new LazySingleton());
        }
        protected LazySingleton(){}
        public void set_t(int n)
        {
            time = n;
        }
        public int get_t()
        {
            return time;
        }
    }

}

public class Singleton
{
    public static int played;
    public static int level;
    public static string name;
    private Singleton() { }

    private static Singleton _instance;
    
    private static readonly object _lock = new object();

    public static Singleton GetInstance(int p, int l, string n)
    {
        if (_instance == null)
        {
            lock (_lock)
            {
                if (_instance == null)
                {
                    _instance = new Singleton();
                    played = p;
                    level = l;
                    name = n;
                }
            }
        }
        return _instance;
    }
}
