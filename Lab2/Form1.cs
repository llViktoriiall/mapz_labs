﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Collections;
using Newtonsoft.Json;

namespace Game_0._1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            string info = "";
            Thread process1 = new Thread(() =>
            {
                Singleton.GetInstance(6, 4, "Ivan2008");
                info = "Hello, " + Singleton.name + ". You are at level " + Singleton.level + " and you already have played " + Singleton.played + " games.\n";
            });
            Thread process2 = new Thread(() =>
            {
                Singleton.GetInstance(7, 7, "SuperPuperOliia");
                info = "Hello, " + Singleton.name + ". You are at level " + Singleton.level + " and you already have played " + Singleton.played + " games.\n";
            });
            richTextBox1.Text = "Thread-safe singleton:\n";
            process1.Start();
            process2.Start();
            process1.Join();
            richTextBox1.Text = richTextBox1.Text + "The singleton after first thread: \n";
            richTextBox1.Text = richTextBox1.Text + info;
            process2.Join();
            richTextBox1.Text = richTextBox1.Text + "The singleton after second thread: \n";
            richTextBox1.Text = richTextBox1.Text + info;




            richTextBox1.Text = richTextBox1.Text + "\nLazy singleton: \n";
            LazySingleton s1 = LazySingleton.Instance();
            LazySingleton s2 = LazySingleton.Instance();
            s1.set_t(30);
            int time = s2.get_t();
            richTextBox1.Text = richTextBox1.Text + "You have " + time + " minutes to complete the round\n";




            richTextBox1.Text = richTextBox1.Text + "\nAbstract fabric: \n";
            FactoryClient s = new FactoryClient();
            s.M_f();
            string t = s.info;
            richTextBox1.Text = richTextBox1.Text + "You have " + s.count2 + " " + t + "\n";
            s.S_m();
            t = s.info;
            richTextBox1.Text = richTextBox1.Text + "You have " + s.count1 + " " + t + "\n";
            s.S_f();
            s.S_f();
            t = s.info;
            richTextBox1.Text = richTextBox1.Text + "You have " + s.count3 + " " + t + "\n";

            richTextBox1.Text = richTextBox1.Text + "\nPrototype: \nOn this level you have next recources: \n";
            Recource r1 = new Recource();
            r1.name = "Oil";
            r1.amount = 0.55;
            r1.price = 6.2;
            string inf = r1.name + " in amount of " + r1.amount + " with price: " + r1.price + "\n";
            richTextBox1.Text = richTextBox1.Text + inf;
            Recource r2 = r1.Copy();
            Recource r3 = r1.Copy();
            r2.name = "Gold";
            inf = r2.name + " in amount of " + r2.amount + " with price: " + r2.price + "\n";
            richTextBox1.Text = richTextBox1.Text + inf;
            r3.price = 0.77;
            inf = r3.name + " in amount of " + r3.amount + " with price: " + r3.price + "\n";
            richTextBox1.Text = richTextBox1.Text + inf;


        }
    }


     //твірні шаблони

    public class Recource
    {
        public double amount;
        public string name;
        public double price;

        public Recource Copy()
        {
            Recource clone = (Recource)this.MemberwiseClone();
            clone.name = String.Copy(name);
            return clone;
        }
    }

    public interface IAbstractFactory//наш інтерфейс з методами створення абстрактних продуктів
    {
        IAbstractProductA CreateProductA();

        IAbstractProductB CreateProductB();
    }

    //метод з інтерфейсу, котрий повертає вміст продукту
    public interface IAbstractProductA
    {
        string WhatContainA();
    }

    //вміст продукту
    internal class ConcreteProductA1 : IAbstractProductA
    {
        public string WhatContainA()
        {
            return " mine";
        }
    }

    //вміст продукту
    internal class ConcreteProductA2 : IAbstractProductA
    {
        public string WhatContainA()
        {
            return " factory";
        }
    }

    //те саме тільки для б
    public interface IAbstractProductB
    {
        string WhatContainB_A(IAbstractProductA collaborator);
    }

    internal class ConcreteProductB1 : IAbstractProductB
    {

        public string WhatContainB_A(IAbstractProductA collaborator)
        {
            var result = collaborator.WhatContainA();
            return "check";//checked
        }
    }

    internal class ConcreteProductB2 : IAbstractProductB
    {

        public string WhatContainB_A(IAbstractProductA collaborator)
        {
            var result = collaborator.WhatContainA();
            return "check";//checked
        }
    }

    //фабрики для комбінування двох абстрактних продуктів в один конкретний
    internal class ConcreteFactory1 : IAbstractFactory
    {
        public IAbstractProductA CreateProductA()
        {
            return new ConcreteProductA1();
        }

        public IAbstractProductB CreateProductB()
        {
            return new ConcreteProductB1();
        }
    }

    internal class ConcreteFactory2 : IAbstractFactory
    {
        public IAbstractProductA CreateProductA()
        {
            return new ConcreteProductA2();
        }

        public IAbstractProductB CreateProductB()
        {
            return new ConcreteProductB2();
        }
    }

    internal class ConcreteFactory3 : IAbstractFactory
    {
        public IAbstractProductA CreateProductA()
        {
            return new ConcreteProductA2();
        }

        public IAbstractProductB CreateProductB()
        {
            return new ConcreteProductB1();
        }
    }
    //клієнт
    internal class FactoryClient
    {
        public string info;
        public int count1 = 0;
        public int count2 = 0;
        public int count3 = 0;
        public void S_m()
        {
            ClientMethod(new ConcreteFactory1());
            count1++;
        }
        public void M_f()
        {
            ClientMethod(new ConcreteFactory2());
            count2++;
        }
        public void S_f()
        {
            ClientMethod(new ConcreteFactory3());
            count3++;
        }
        public void ClientMethod(IAbstractFactory factory)
        {
            var productA = factory.CreateProductA();
            var productB = factory.CreateProductB();
            info = productB.WhatContainB_A(productA);
        }
    }


    public class LazySingleton
    {
        private static int time = 0;
        private static LazySingleton instance;
        public static LazySingleton Instance()
        {
            return instance ?? (instance = new LazySingleton());
        }
        protected LazySingleton() { }
        public void set_t(int n)
        {
            time = n;
        }
        public int get_t()
        {
            return time;
        }
    }

}


public class Singleton
{
    public static int played;
    public static int level;
    public static string name;
    private Singleton() { }

    private static Singleton _instance;

    private static readonly object _lock = new object();

    public static Singleton GetInstance(int p, int l, string n)
    {
        if (_instance == null)
        {
            lock (_lock)
            {
                if (_instance == null)
                {
                    _instance = new Singleton();
                    played = p;
                    level = l;
                    name = n;
                }
            }
        }
        return _instance;
    }
}

